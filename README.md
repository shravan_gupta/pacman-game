# Pacman Game

This project was assignment 1 for CMPT 310(Artificial Intelligence Survey). Pac-Man agent would find paths through his maze world, both to reach a particular location and to collect food efficiently. I build general search algorithms and applied them to Pac-Man scenarios.

Files I edited:
* **search.py**:          Where all of the search algorithms reside.
* **searchAgents.py**:    Where all of the search-based agents  reside.

Supporting files:
* **pacman.py**:   The main file that runs Pac-Man games. This file describes a Pac-Man GameState type.
* **game.py**:     The logic behind how the Pac-Man world works. This file describes several supporting types like AgentState, Agent, Direction, and Grid.
* **util.py**:     Useful data structures for implementing search algorithms
* **graphicsDisplay.py**:      Graphics for Pac-Man
* **graphicsUtils.py**:        Support for Pac-Man graphics
* **textDisplay.py**:          ASCII graphics for Pac-Man
* **ghostAgents.py**:          Agents to control ghosts
* **keyboardAgents.py**:       Keyboard interfaces to control Pac-Man
* **layout.py**:               Code for reading layout files and storing their contents


